# We require at least version 7 of powershell
#Requires -Version 7
# Make sure all errors will stop the module logic
$ErrorActionPreference = 'Stop'

function Invoke-BugReproduction {
  [CmdletBinding()]
	param
	(
    [Parameter(Mandatory = $false)]
    [AllowNull()]
    [AllowEmptyString()]
    [string]
    $ValueToIgnore = '∅'
  )
  Write-Host ">"
  Write-Host "> Writing the unicode strings to hex_compare.txt"
  Write-Host "> See job artifacts."
  Write-Host ">"

  Write-Output "> unicode from .gitlab-ci.yml:"        > hex_compare.txt
  $env:GL_Test1 | Format-Hex                         >> hex_compare.txt
  Write-Output "> unicode being used for comparison:"  >> hex_compare.txt
  $ValueToIgnore | Format-Hex                        >> hex_compare.txt

  Write-Host "> comparing the values - they should match"

  if ($env:GL_Test1 -ne $ValueToIgnore) {
    Write-Host "> values do not match"
  }
  if ($env:GL_Test1 -eq $ValueToIgnore) {
    Write-Host "> values match"
  }
}
